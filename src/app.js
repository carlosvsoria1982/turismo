require('dotenv').config()  //debe estar en primer lugar, se utiliza para compartir los puertos en un mismo servidor
const app = require('./express')
// const mongoose = require('mongoose');
const morgan = require('morgan')

const { appConfig, db } = require('./config/config')    // tomo las variables de configuración globales


// Importando la configuracion de conexion a la BD
const connectDb = require('../src/config/database')
// realizando la conexión a la base de datos
connectDb(db)
  .then(db => console.log('Conectado a la base de datos'))
  .catch(err => console.log(err, 'error1'))

// Settings

app.set('port', appConfig.port)

app.get('port')

// Midlewares
app.use(morgan('dev'))

app.listen(appConfig.port, () => {
  console.log(`servicio iniciado en puerto: ${appConfig.port}`)
})

