const Commercial = require('../models/places')
const { appConfig } = require('../config/config')
const propietary = require('../models/propietary')

async function addPlaces(req, res) {

}

async function indexPlaces(req, res) {
  
}

async function deletePlaces(req, res) {
  
}

async function setData(req, res) {
  
}

async function addImage(req, res) {
  
}

async function updatePlaces(req, res) {

}

module.exports = {
  indexPlaces,
  addPlaces,
  deletePlaces,
  setData,
  updatePlaces,
  addImage
}
