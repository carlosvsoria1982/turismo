const { appConfig } = require('../config/config')
const Propietary = require('../models/propietary')
const Commercial = require('../models/commercial')
const Province = require('../models/province')
const Stay = require('../models/stay')
const Location = require('../models/location')
const Experience = require('../models/experience')
const { response } = require('express')

async function Image(params, body, collection, file, res) {
  console.log("ejecutando addImage")
  // console.log(collection)
  console.log({ params, body, collection })
  const { id } = params
  const filter = { _id: id }
  const {
    images,
    imageMainText,
    imageSubText,
    imagePropietary
  } = body
  var yesexits = ""
  var toSave =""
  var exits =""
  switch (collection) {
    case 'propietary':
      const propietary = Propietary({
        images
      })
      exits = await Propietary.exists(filter)
      yesExits = exits
      break;
    case 'commercial':
      const commercial = Commercial({
        images
      })
      exits = await Commercial.exists(filter)
      yesExits = exits
      break;
    case 'province':
      const province = Province({
        images
      })
      exits = await Province.exists(filter)
      yesExits = exits
      break;
    case 'stay':
      const stay = Stay({
        images
      })
      exits = await Stay.exists(filter)
      yesExits = exits
      break;
    case 'location':
      const location = Location({
        images
      })
      exits = await Location.exists(filter)
      yesExits = exits
      break;
    case 'experience':
      const experience = Experience({
        images
      })
      exits = await Experience.exists(filter)
      yesExits = exits
      break;

  }
  try {
    if (yesExits) {
      console.log("id correcto:", id)
      toSave = { imageMainText: imageMainText, imageSubText: imageSubText, imagePropietary: imagePropietary }
      // console.log({toSave})
      if (file) {
        console.log("------------------------")
        const { filename } = file
        const { host, port } = appConfig
        const new_url = `${host}:${port}/public/${filename}`
        // console.log({ url_pusheada: new_url })
        // console.log("------------------------------------")
        toSave.imageUrl = new_url
        // console.log(toSave)

      }
      else {
        console.log("error1")
      }
    }
  }
  catch (err) {
    console.log(err)
    res.status(500).json({ status: 'Error Desconocido' })
  }
  try {
    var doc = ""
        switch (collection) {
          case 'propietary':
            doc = await Propietary.findOneAndUpdate(filter, { $addToSet: { images: [toSave] } }, (error, book) => {
              anddlerError(error,res,collection)
 
            })
            break;
          case 'commercial':
            doc = await Commercial.findOneAndUpdate(filter, { $addToSet: { images: [toSave] } }, (error, book) => {
              anddlerError(error,res, collection)
              
            })
            break;
          case 'province':
            doc = await Province.findOneAndUpdate(filter, { $addToSet: { images: [toSave] } }, (error, book) => {
              anddlerError(error,res, collection)
              
            })
            break;
          case 'stay':
            doc = await Stay.findOneAndUpdate(filter, { $addToSet: { images: [toSave] } }, (error, book) => {
              anddlerError(error,res,collection)
 
            })
            break;
          case 'location':
            doc = await Location.findOneAndUpdate(filter, { $addToSet: { images: [toSave] } }, (error, book) => {
              anddlerError(error,res, collection)
              
            })
            break;
          case 'experience':
            doc = await Experience.findOneAndUpdate(filter, { $addToSet: { images: [toSave] } }, (error, book) => {
              anddlerError(error,res, collection)
              
            })
            break;
  
         }
  } catch (error) {
    console.log(error)
  }
}

function anddlerError(error, res, collection){
  if (error) {
    console.log(`Problemas al intentar almacenar la nueva imagen`, error);
    res.status(500).send(err);
    // console.log("book: ", book)
  }
  else {
    console.log(`nueva imagen almacenada satisfactoriamente`);
    // console.log("doc: ", doc)
    res.status(300).json({ status: `Updated ${collection} sucessfull: ` })
  }
}

module.exports = {
  Image
}