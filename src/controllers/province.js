// const province = require('../models/province')
const Province = require('../models/province')
const { appConfig } = require('../config/config')
// const property = require('../models/property');
// const { response } = require('express');
const {Image} = require('../controllers/functions')


async function addProvince(req, res) {

    try {
        console.log("ejecutando addProvince")
        const {
            provinceName,
            urlName,
            mainTextProvince,
            subtittleProvince,
        } = req.body

        var province = Province({
            provinceName,
            urlName,
            mainTextProvince,
            subtittleProvince,
        })


        // defino que cuando se crea una provincia, estos valores deben ser cero
        const provinceExits = await Province.exists({ provinceName: provinceName })
        if (provinceExits) {
            console.log("Intentando almacenar una provincia que ya existe")
            res.status(200).send({ status: 'la provincia ya existe en la base de datos' })
        }
        else {

            const provinceStored = await province.save()
            res.status(200).send({ success: provinceStored })
            console.log(provinceStored)
            console.log(`nuevo province: ${provinceName} agregado correctamente con id: ${provinceStored._id}`)
        }
    } catch (error) {
        res.status(500).send(console.log(error.message))

    }


}

async function indexProvince(req, res) {
    console.log('entramos en index Province')
    // console.log("query",req.query)
    // console.log("params",req.params)
    console.log("body",req.body)
    const filter = req.body.data
    // console.log(filter)
    
    
    const province = await Province.find(filter)
    // console.log(province)
    res.status(200).json({ province: province })
}

async function province(req, res) {
    const { id } = req.params

    try {
        const provinceExits = await Province.exists({ _id: id })        //acá hay un issue cdo se consulta un id no válido
        if (provinceExits == true) {
            console.log(`la provincia on id: ${id} si existe ${provinceExits}`)
            const province = await Province.findById({ _id: id })
            console.log(province)
            console.log('funcion Province')
            res.status(200).json({ province: [province] })
        } else {
            console.log('Id no encontrado')
            res.status(300).json({ status: 'Id de province no encontrado' })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ status: 'error desconocido' })
    }

}

async function deleteProvince(req, res) {
    const { id } = req.params
    try {
        const provinceExits = await Province.exists({ _id: id })
        console.log(provinceExits)
        if (provinceExits == true) {
            console.log("id correcto:", id)
            const province = await Province.deleteOne({ _id: id })
            // propietary.save()
            console.log(province)
            res.status(300).json({ status: 'Deleted province sucessfull' })
            console.log("Deleted province sucessfull")

        }
        else {
            console.log('Id no encontrado')
            res.status(300).json({ status: 'Id de province no encontrado' })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ status: 'error desconocido' })
    }




}


async function updateProvince(req, res) {
    console.log("------------------------")
    console.log("ejecutando updateProvince")
    const { id } = req.params
    console.log({ body: req.params })
    console.log("------------------------")
    console.log({ body: req.body })
    console.log("------------------------")
    const {
        provinceName,
        urlName,
        imageUrl,
        mainTextProvince,
        subtittleProvince
    } = req.body

    const province = Province({
        provinceName,
        urlName,
        imageUrl,
        mainTextProvince,
        subtittleProvince
    })

    const filter = { _id: id }
    try {
        const provinceExits = await Province.exists(filter)
        if (provinceExits == true) {
            console.log(`La provincia con Id: ${id} existe`)
            console.log("------------------------")
            console.log(`urlName: ${urlName}`)
            if (req.file) {
                console.log("------------------------")
                const { filename } = req.file
                const { host, port } = appConfig
                const new_url = `${host}:${port}/public/${filename}`
                console.log({ url_pusheada: new_url })
                console.log("------------------------------------")
                const doc = await Province.findOneAndUpdate(filter, { $set: { provinceName: provinceName, mainTextProvince: mainTextProvince, urlName: urlName, subtittleProvince: subtittleProvince }, $addToSet: { imageUrl: [new_url] } }, (err, book) => {
                    if (err) {
                        console.log(`Problemas al intentar almacenar el nuevo nombre de la provincia ${provinceName}`, err);
                        res.status(500).send(err);
                        console.log("book: ", book)
                    } else {
                        console.log(`Nuevo nombre de la provincia ${provinceName} almacenado satisfactoriamente`);
                        console.log("doc: ", doc)
                        res.status(300).json({ status: `Updated province sucessfull: ` })
                    }
                })
                console.log("--------------Updated province sucessfull--------------------------")
            } else {

                const doc = await Province.findOneAndUpdate(filter, { provinceName: provinceName, mainTextProvince: mainTextProvince, urlName: urlName, subtittleProvince: subtittleProvince }, (err, book) => {
                    if (err) {
                        console.log(`Problemas al intentar almacenar el nuevo nombre de la provincia ${provinceName}`, err);
                        res.status(500).send(err);
                        console.log("book: ", book)
                    } else {
                        console.log(`Nuevo nombre de la provincia ${provinceName} almacenado satisfactoriamente`);
                        res.status(300).json({ status: `Updated province sucessfull: ` })
                    }
                })
            }
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ status: 'error desconocido' })
    }
}

async function addImage(req, res) {
    const collection = "province"
    Error1 = Image(req.params, req.body, collection, req.file, res)
}

async function setData(req, res) {
    console.log(req.body)
    console.log(req.params)
  
    console.log("ejecutando setData...")
    const { id } = req.params
    const filter = { _id: id }
  
    const {
      locationName,
    } = req.body
    const province = Province({
      locationName,
    })
    try {
      const provinceExits = await Province.exists(filter)
      if (provinceExits) {
        console.log("id correcto:", id)
        const toSave = {}
        // comparative to update multi values on province data

        if (typeof locationName !== "undefined") {
          toSave.location = locationName
        }

        console.log(toSave)
        const doc = await Province.findOneAndUpdate(filter, { $addToSet:  toSave }, (err, book) => {
          if (err) {
            console.log(`Problemas al intentar actualizar`, err);
            res.status(500).send(err);
            // console.log("book: ", book)
          } else {
            console.log(`nueva actualización satisfactoria`);
            
            res.status(300).json({ status: `Updated commercial sucessfull: ` })
          }
        }
        )
        console.log("doc: ", doc)
      }
    }
    catch (err) {
      console.log(err)
      res.status(500).json({ status: 'Error Desconocido' })
    }
  }

module.exports = {
    province,
    indexProvince,
    addProvince,
    deleteProvince,
    updateProvince,
    addImage,
    setData
}
