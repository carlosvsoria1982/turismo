const Location = require('../models/location')
const {Image} = require('../controllers/functions')

async function addLocation(req, res) {

    try {
        console.log("ejecutando addProvince")
        const {
            locationName,
            locationNameUrl,
            mainTextLocation,
            subTextLocation
        } = req.body

        const location = Location({
            locationName,
            locationNameUrl,
            mainTextLocation,
            subTextLocation
        })

        const locationExits = await Location.exists({ locationName: locationName })
//
        if (locationExits) {
            console.log("Intentando almacenar una localidad ya existente")
            res.status(200).send({ status: `Ya existe en la base de datos con el nombre de: ${locationName}`}) //arreglar los demás para que respondan así
        }
        else {
            const locationStored = await location.save()
            res.status(200).send({ success: locationStored })
            console.log(locationStored)
            console.log(`nueva actividad agregada correctamente: ${locationName}`)
        }

    } catch (error) {
        res.status(500).send(console.log(error.message))

    }


}

async function indexLocation(req, res) {

    console.log('entramos en index')
    console.log(req.body)
    const location = await Location.find({})
    res.status(200).json({ location:location })
}

async function deleteLocation(req, res) {
    const { id } = req.params
    try {

        const locationExits = await Location.exists({ _id: id })
        console.log(locationExits)
        if (locationExits == true) {
            console.log("id correcto:", id)
            const location = await Location.deleteOne({ _id: id })
            // propietary.save()
            console.log(location)
            res.status(300).json({ status: 'Deleted location sucessfull' })
            console.log("Deleted location sucessfull")

        }
        else {
            console.log('Id no encontrado')
            res.status(300).json({ status: 'Id de location no encontrado' })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ status: 'error desconocido' })
    }
}

async function addImage(req, res) {
    const collection = "location"
    Error1 = Image(req.params, req.body, collection, req.file, res)
  }

module.exports = {
    indexLocation,
    addLocation,
    deleteLocation,
    addImage
}
