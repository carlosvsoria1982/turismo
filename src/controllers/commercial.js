const Commercial = require('../models/commercial')
// const { appConfig } = require('../config/config')
const location =require('../models/location')
const propietary = require('../models/propietary')
const {Image} = require('../controllers/functions')
const ParseGeoLocation = require('../Commons/ParseGeoLocation')

async function addCommercial(req, res) {
  console.log("ejecutando addCommercial")
  try {
    console.log("ejecutando addCommercial")
    const {
      commercialName,
      commercialType,
      urlCommercialName,
      email,
      phoneNumber,
      address,
      contactWhatsApp,
      propietary,
      commercialMainText,
      comercialSubText,
      hoursOpen,
      webSite,
      social,
      active
    } = req.body
    console.log(req.body)

    var {geoLocation} =req.body
    console.log(geoLocation)
    geoLocation = ParseGeoLocation(geoLocation)
    console.log(geoLocation)
    const commercial = Commercial({
      commercialName,
      commercialType,
      urlCommercialName,
      email,
      phoneNumber,
      address,
      contactWhatsApp,
      propietary,
      commercialMainText,
      comercialSubText,
      hoursOpen,
      webSite,
      social,
      active
    })
    console.log("----------------------------------")

    const commercialExits = await Commercial.exists({ commercialName: commercialName })

    if (commercialExits) {
        console.log("Intentando almacenar un comercio que ya existente")
        res.status(200).send({ status: `Ya existe en la base de datos con el nombre de: ${commercialName}`}) //arreglar los demás para que respondan así
    }
    else {
        const commercialStored = await commercial.save()
        // res.status(200).send({ success: commercialStored })
        res.status(200).send({ sucess: "ok" })
        console.log(commercialStored)
        console.log(`nuevo comericio agregado correctamente: ${commercialName}`)
    }
  
    
  } catch (error) {
    res.status(500).send(console.log(`Error: ${error.message}`))
  }


}

async function indexCommercial(req, res) {
  console.log('entramos en index de comercial')
  const info = req.body
  console.log("info",info)
  var filter = req.query
  var typeFilter = info.typeFilter
  var commercial = {}
  var newFilter = {}
  
  switch (typeFilter)
    {
    case 'single':
      {
      var id = info.data
      // newFilter = {_id : id}
      newFilter = info.data
      commercial = await Commercial.find(newFilter).populate('location').populate('propietary').populate('province').exec()
      // commercial = await Commercial.find(newFilter).populate('propietary').populate('stays').populate('province').populate('location').exec((err, commercial) => console.log(err))
      console.log("holas aqui...")
      console.log(commercial)
      break;
      }
    case 'multi':
      {
      var nameFilter = info.nameFilter
      switch (nameFilter)
        {
        case 'stays':
          newFilter = info.data.urlName
          commercial = await Commercial.aggregate(
            [{ $unwind: "$stays" },
            {$lookup:
              {
                  from: "provinces",
                  localField: "province",
                  foreignField: "_id",
                  as: "info"
               }
            },
            {$lookup:
            {
              from: "stays",
              localField: "stays",
              foreignField: "_id",
              as: "stays_1"
            }
            },
            {$lookup:
            {
              from: "propietaries",
              localField: "propietary",
              foreignField: "_id",
              as: "propietario"
           }
           },
           {$match:{'info.urlName': newFilter}}
            ])
          commercial.map(comercial => {
            delete comercial.stays
            comercial.stays= comercial.stays_1[0]
            delete comercial.stays_1
            delete comercial.province
            comercial.province= comercial.info[0].urlName
            delete comercial.info
            delete comercial.propietary
            comercial.propietary= comercial.propietario[0].urlname
            delete comercial.propietario
          })
          console.log("holas acá")
          console.log(commercial)
          
          break;
        case 'experiences':
        
          break;
        case 'culinary':
         
          break;
        
        default:
          break;
        }
      break;
      }

    }
  res.status(200).json({commercial})
}

async function deleteCommercial(req, res) {
  const { id } = req.params
  try {

    const activityExits = await Commercial.exists({ _id: id })
    console.log(activityExits)
    if (activityExits == true) {
      console.log("id correcto:", id)
      const activity = await Commercial.deleteOne({ _id: id })
      // propietary.save()
      console.log(activity)
      res.status(300).json({ status: 'Deleted Coomercial sucessfull' })
      console.log("Deleted Coomercial sucessfull")

    }
    else {
      console.log('Id no encontrado')
      res.status(300).json({ status: 'Id de Coomercial no encontrado' })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ status: 'error desconocido' })
  }
}

async function setData(req, res) {
  // console.log(req.params)
  console.log("ejecutando setData...")
  console.log(req.body)
  const { id } = req.params
  const filter = { _id: id }

  const {
    province,
    propietary,
    location,
    stays,
    experiences
  } = req.body
  const commercial = Commercial({
    province,
    propietary,
    location,
    stays,
    experiences
  })
  try {
    const commercialExits = await Commercial.exists(filter)
    if (commercialExits) {
      console.log("id correcto:", id)
      const toSave = {}
      // comparative to update multi values on commercial data
      if (typeof province !== "undefined") {
        toSave.province = province
      }
      if (typeof propietary !== "undefined") {
        toSave.propietary = propietary
      }
      if (typeof location !== "undefined") {
        toSave.location = location
      }
      if (typeof stays !== "undefined") {
        toSave.stays = stays
      }
      if (typeof experiences !== "undefined") {
        toSave.experiences = experiences
      }
      console.log(toSave)
      const doc = await Commercial.findOneAndUpdate(filter, { $addToSet:  toSave }, (err, book) => {
        if (err) {
          console.log(`Problemas al intentar actualizar`, err);
          res.status(500).send(err);
          // console.log("book: ", book)
        } else {
          console.log(`nueva actualización satisfactoria`);
          
          res.status(300).json({ status: `Updated commercial sucessfull: ` })
        }
      }
      )
      console.log("doc: ", doc)
    }
  }
  catch (err) {
    console.log(err)
    res.status(500).json({ status: 'Error Desconocido' })
  }
}

async function addImage(req, res) {
  const collection = "commercial"
  Error1 = Image(req.params, req.body, collection, req.file, res)
}

async function updateCommercial(req, res) {
  console.log("ejecutando updateCommercial")
  const { id } = req.params
  var {
    commercialName,
      commercialType,
      urlCommercialName,
      email,
      phoneNumber,
      address,
      contactWhatsApp,
      propietary,
      commercialMainText,
      comercialSubText,
      hoursOpen,
      webSite,
      social,
      active
  } = req.body

  const commercial = Commercial({
    commercialName,
    commercialType,
    urlCommercialName,
    email,
    phoneNumber,
    address,
    contactWhatsApp,
    propietary,
    commercialMainText,
    comercialSubText,
    hoursOpen,
    webSite,
    social,
    active
  })
  geoLocation = ParseGeoLocation(geoLocation)
  const filter = { _id: id }
  try {
      const commercialExits = await Commercial.exists(filter)
      if (commercialExits == true) {
          console.log(`La commercial con Id: ${id} existe`)
          console.log("------------------------")
          
          const toSave = { 	commercialName,
            commercialType,
            urlCommercialName,
            email,
            phoneNumber,
            address,
            contactWhatsApp,
            propietary,
            commercialMainText,
            comercialSubText,
            hoursOpen,
            webSite,
            social,
            active }

              const doc = await Commercial.findOneAndUpdate(filter, toSave, (err, book) => {
                  if (err) {
                      console.log(`Problemas al intentar actualizar los datos del comercio: ${commercialName}`, err);
                      res.status(500).send(err);
                      console.log("book: ", book)
                  } else {
                      console.log(`Datos del comercio ${commercialName} actuaizados satisfactoriamente`);
                      res.status(300).json({ status: `Updated province sucessfull: ` })
                  }
              })
          
      }
  } catch (error) {
      console.log(error)
      res.status(500).json({ status: 'error desconocido' })
  }
}

async function test(req, res) {
  console.log("ejecutando test")
  const filter = req.query
  console.log(filter)
  const commercial = await Commercial.find({stays:{$exists:true,$ne:[]}})
  console.log(commercial)
   res.status(200).json({ commercial: commercial })

}

module.exports = {
  indexCommercial,
  addCommercial,
  deleteCommercial,
  setData,
  updateCommercial,
  addImage,
  test
}
