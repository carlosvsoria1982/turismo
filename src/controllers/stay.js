const Stay = require('../models/stay')
const {Image} = require('../controllers/functions')

async function addStay(req, res) {
console.log(req)
    try {
        console.log("ejecutando addStay");
   
        
        const {
            stayType,
            stayName,
            urlStayName,
            mainText,
            subText,
            ocupancy,
            beds,
            rooms,
            kitchen,
            microWave,
            oven,
            grills,
            washingMachine,
            wifi,
            parking,
            airConditioning,
            heating,
            smartTV,
            fridge,
            breakfast,
            plate,
            laundryService,
            armchair,
            checkIn,
            checkOut,
        } = req.body

        const stay = Stay({
            stayType,
            stayName,
            urlStayName,
            mainText,
            subText,
            ocupancy,
            beds,
            rooms,
            kitchen,
            microWave,
            oven,
            grills,
            washingMachine,
            wifi,
            parking,
            airConditioning,
            heating,
            smartTV,
            fridge,
            breakfast,
            plate,
            laundryService,
            armchair,
            checkIn,
            checkOut,
        })
        const stayExits = await Stay.exists({ stayName: stayName })
        if (stayExits) {
            console.log("Intentando almacenar una experiencia ya existente")
            res.status(200).send({ status: `Ya existe en la base de datos con el nombre de: ${stayName}`}) //arreglar los demás para que respondan así
        }
        else {
            const stayStored = await stay.save()
            res.status(200).send({ success: stayStored })
            console.log(stayStored)
            console.log(`nueva actividad agregada correctamente: ${stayName}`)
        }
    } catch (error) {
        res.status(500).send(console.log(error.message))
    }
}

async function indexStay(req, res) {
    console.log('entramos en index de Stay')
    console.log(req.query)
    const filter = req.query
    const stay = await Stay.find(filter)
    console.log(stay)
    res.status(200).json({ stay: stay })
}

async function deleteStay(req, res) {
    const { id } = req.params
    try {

        const stayExits = await Stay.exists({ _id: id })
        console.log(stayExits)
        if (stayExits == true) {
            console.log("id correcto:", id)
            const stay = await Stay.deleteOne({ _id: id })
            // propietary.save()
            console.log(stay)
            res.status(300).json({ status: 'Deleted stay sucessfull' })
            console.log("Deleted stay sucessfull")

        }
        else {
            console.log('Id no encontrado')
            res.status(300).json({ status: 'Id de stsy no encontrado' })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ status: 'error desconocido' })
    }
}
async function addImage(req, res) {
    const collection = "stay"
    Error1 = Image(req.params, req.body, collection, req.file, res)
  }
async function updateStay(req, res) {
    console.log("ejecutando updateStay")
  const { id } = req.params
  const {
    stayType,
    stayName,
    urlStayName,
    mainText,
    subText,
    ocupancy,
    beds,
    rooms,
    kitchen,
    microWave,
    oven,
    grills,
    washingMachine,
    wifi,
    parking,
    airConditioning,
    heating,
    smartTV,
    fridge,
    breakfast,
    plate,
    laundryService,
    armchair,
    checkIn,
    checkOut,
} = req.body

// const stay = Stay({
//     stayType,
//     stayName,
//     urlStayName,
//     mainText,
//     subText,
//     ocupancy,
//     beds,
//     rooms,
//     kitchen,
//     microWave,
//     oven,
//     grills,
//     washingMachine,
//     wifi,
//     parking,
//     airConditioning,
//     heating,
//     smartTV,
//     fridge,
//     breakfast,
//     plate,
//     laundryService,
//     checkIn,
//     checkOut,
// })

  const filter = { _id: id }
  try {
      const stayExits = await Stay.exists(filter)
      if (stayExits == true) {
          console.log(`La commercial con Id: ${id} existe`)
          console.log("------------------------")
          var temp = req.body
          console.log(temp)
          var toSave = {}
          for (var key in temp)
          {
            toSave[key]=temp[key]
            console.log(key)
          }
          const stay = Stay(toSave)

              const doc = await Stay.findOneAndUpdate(filter, toSave, (err, book) => {
                  if (err) {
                      console.log(`Problemas al intentar actualizar los datos del hospedaje: ${stayName}`, err);
                      res.status(500).send(err);
                      console.log("book: ", book)
                  } else {
                      console.log(`Datos del comercio ${stayName} actuaizados satisfactoriamente`);
                      res.status(300).json({ status: `Updated stay sucessfull: ` })
                  }
              })
          
      }
  } catch (error) {
      console.log(error)
      res.status(500).json({ status: 'error desconocido' })
  }
  }

module.exports = {
    indexStay,
    addStay,
    deleteStay,
    addImage,
    updateStay
}
