const Propietary = require('../models/propietary')
// const activity = require('../models/activity')
// const property = require('../models/property');
const {Image} = require('../controllers/functions')
// const { response } = require('express');
// const { appConfig } = require('../config/config')
const ParseGeoLocation = require('../Commons/ParseGeoLocation')
const DetectStringSocial = require('../Commons/DetectStringSocial')
// const { parse } = require('dotenv/types')



async function AddPropietary(req, res) {

  try {
    console.log("ejecutando addPropietary")
    console.log(req.body)
    const {
      propietaryName,
      languages,
      urlname,
      email,
      phoneNumber,
      proptietaryText,
      bio,
      webSite,
      social,
      provinceName,
      locationName,
      stays,
      experiences,
      geoLocation,
      active
    } = req.body

    const propietary = Propietary({
      propietaryName,
      languages,
      urlname,
      email,
      phoneNumber,
      proptietaryText,
      bio,
      webSite,
      social,
      provinceName,
      locationName,
      stays,
      experiences,
      geoLocation,
      active
    })
    console.log("----------------------------------")

    const propietaryStored = await propietary.save()
    const response = await propietaryStored.populate('provinceName').execPopulate();  // respuesta populada
    console.log(`nuevo usuario agregado correctamente ${response}`)
    res.status(200).send({ success: response })
  } catch (error) {
    res.status(500).send(console.log(`Error: ${error.message}`))
  }
}

async function indexFunc(req, res) {
  const filter = req.query
  console.log('entramos en index de propietarios')
  console.log(req.query)
  const propietary = await Propietary.find(filter)
    console.log(propietary)
    res.status(200).json({ propietary: propietary })
  

}

async function deletePropietary(req, res) {
  const { id } = req.params
  try {

    const propietaryExit = await Propietary.exists({ _id: id })
    console.log(propietaryExit)
    if (propietaryExit == true) {
      console.log("id correcto:", id)
      const propietary = await Propietary.deleteOne({ _id: id })
      // propietary.save()
      console.log(propietary)
      res.status(300).json({ status: `Deleted sucessfull _id: ${id}` })
      console.log(`Deleted sucessfull _id: ${id}`)
    }
    else {
      console.log(`Id: ${id} no encontrado`)
      res.status(300).json({ status: `Id: ${id} no encontrado` })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ status: 'error desconocido' })
  }




}
/* Function for get individual propietary throw id*/
async function getOnePropietary(req, res) {
  const { id } = req.query
  console.log("entrando al la función getOnePropietary")
  try {
    const propietaryExits = await Propietary.exists({ _id: id })        //acá hay un issue cdo se consulta un id no válido
    if (propietaryExits == true) {
      console.log(`El propietario con id: ${id} si existe ${propietaryExits}`)
      const propietary = await Propietary.findById({ _id: id }).populate('provinceName').exec((err, propietary) => {
        console.log(propietary)
        res.status(200).json({ propietary: propietary })
      })
      // console.log(propietary)
      // console.log('funcion Propietario')
      // res.status(200).json({ propietary: [propietary] })
    } else {
      console.log('Id no encontrado')
      res.status(300).json({ status: 'Id de propietario no encontrado' })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ status: 'error desconocido' })
  }

}
async function addImage(req, res) {
  const collection = "propietary"
 Error1 = Image(req.params, req.body, collection, req.file, res)

  }

  async function updatePropietary(req, res) {
    console.log("ejecutando updatePropietary")
    const { id } = req.params
    // console.log(req.body)
    var {
      propietaryName,
      languages,
      urlname,
      email,
      phoneNumber,
      proptietaryText,
      bio,
      webSite,
      social,
      geoLocation,
      active
    } = req.body

    geoLocation = ParseGeoLocation(geoLocation)
    social = DetectStringSocial(social)
    const filter = { _id: id }
    try {
        const propietaryExits = await Propietary.exists(filter)
        if (propietaryExits == true) {
            console.log(`La commercial con Id: ${id} existe`)
            console.log("------------------------")
            
            const toSave = {
                propietaryName,
                languages:languages,
                urlname:urlname,
                email:email,
                phoneNumber:phoneNumber,
                proptietaryText:proptietaryText,
                bio:bio,
                webSite:webSite,
                social:social,
                geoLocation:geoLocation,
                active:active
              }
  
                const doc = await Propietary.findOneAndUpdate(filter, toSave, (err, book) => {
                    if (err) {
                        console.log(`Problemas al intentar actualizar los datos del propietario: ${propietaryName}`, err);
                        res.status(500).send(err);
                        console.log("book: ", book)
                    } else {
                        console.log(`Datos del propietario ${propietaryName} actuaizados satisfactoriamente`);
                        res.status(300).json({ status: `Updated propietary sucessfull: ` })
                    }
                })
                console.log(doc)
            
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ status: 'error desconocido' })
    }
  }

module.exports = {
  getOnePropietary,
  indexFunc,
  AddPropietary,
  deletePropietary,
  addImage,
  updatePropietary
}
