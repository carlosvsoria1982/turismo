const Experience = require('../models/experience')
const {Image} = require('../controllers/functions')

async function addExperience(req, res) {

    try {
        console.log("ejecutando addExperience")
        const {
            experienceName,
            experienceType,
            experienceNameUrl,
            experienceMainText,
            experienceSubText,
            review,
            ageForExperience,
            experienceTime,
            active
        } = req.body

        const experience = Experience({
            experienceName,
            experienceType,
            experienceNameUrl,
            experienceMainText,
            experienceSubText,
            review,
            ageForExperience,
            experienceTime,
            active
        })

        const experienceExits = await Experience.exists({ experienceName: experienceName })

        if (experienceExits) {
            console.log("Intentando almacenar una experiencia ya existente")
            res.status(200).send({ status: `Ya existe en la base de datos con el nombre de: ${experienceName}`}) //arreglar los demás para que respondan así
        }
        else {
            const experienceStored = await experience.save()
            res.status(200).send({ success: experienceStored })
            console.log(experienceStored)
            console.log(`nueva actividad agregada correctamente: ${experienceName}`)
        }

    } catch (error) {
        res.status(500).send(console.log(error.message))

    }


}

async function indexExperience(req, res) {
    console.log('entramos en index de experience')
    console.log(req.query)
    const filter = req.query
    const experience = await Experience.find(filter)
    res.status(200).json({ experience: experience })
}

async function deleteExperience(req, res) {
    const { id } = req.params
    try {
        const experienceExits = await Experience.exists({ _id: id })
        console.log(experienceExits)
        if (experienceExits == true) {
            console.log("id correcto:", id)
            const experience = await Experience.deleteOne({ _id: id })
            // propietary.save()
            console.log(experience)
            res.status(300).json({ status: 'Deleted experience sucessfull' })
            console.log("Deleted experience sucessfull")
        }
        else {
            console.log('Id no encontrado')
            res.status(300).json({ status: 'Id de experience no encontrado' })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({ status: 'error desconocido' })
    }
}
async function addImage(req, res) {
    const collection = "experience"
    Error1 = Image(req.params, req.body, collection, req.file, res)
  }

  async function setData(req, res) {
    console.log(req.body)
    // console.log(req.params)
  
    console.log("ejecutando setData...")
    const { id } = req.params
    const filter = { _id: id }
  
    const {
      province,
      location
    } = req.body
    const experience = Experience({
        province,
        location
    })
    try {
      const experienceExits = await Experience.exists(filter)
      if (experienceExits) {
        console.log("id correcto:", id)
        const toSave = {}
        // comparative to update multi values on commercial data
        if (typeof province !== "undefined") {
          toSave.province = province
        }
        if (typeof location !== "undefined") {
          toSave.location = location
        }

        console.log(toSave)
        const doc = await Experience.findOneAndUpdate(filter, { $addToSet:  toSave }, (err, book) => {
          if (err) {
            console.log(`Problemas al intentar actualizar`, err);
            res.status(500).send(err);
            // console.log("book: ", book)
          } else {
            console.log(`nueva actualización satisfactoria`);
            
            res.status(300).json({ status: `Updated commercial sucessfull: ` })
          }
        }
        )
        console.log("doc: ", doc)
      }
    }
    catch (err) {
      console.log(err)
      res.status(500).json({ status: 'Error Desconocido' })
    }
  }

module.exports = {
    indexExperience,
    addExperience,
    deleteExperience,
    addImage,
    setData
}
