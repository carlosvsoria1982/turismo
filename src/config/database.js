// console.log('ingresando a la configuracion de la base de datos')
// Cargando el modulo de mongoose
const mongoose = require('mongoose')
// Configurando la conexion para MongoDB, Debemos indicar el puerto y la IP de nuestra BD

async function connectDb ({ host, port, dbName }) {
    console.log("nombre de la db",dbName)
  const uri = `mongodb://${host}:${port}/${dbName}`
  mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
}
// mongoose.connection.on('open', () => console.log('db connected'))   //Acá tengo duplicado el msj de conexion a la base de datos, hay que elegir cual es el mejor mensaje o más eficiente.
mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise
module.exports = connectDb
