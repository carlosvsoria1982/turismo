// Aquí se almacenan las configuraciones de la aplicación, las variables son tomadas desde el archivo .env
const config = {
  appConfig: {
    host: process.env.APP_HOST,
    port: process.env.APP_PORT || 8081
  },
  db: {
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    dbName: process.env.DB_NAME
  }
}
// console.log(config)
module.exports = config
