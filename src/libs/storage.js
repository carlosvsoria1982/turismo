const path = require('path'); // path for cut the file extension
// se almacena la configuración de multer
const multer = require('multer')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './src/storage/imgs')
  },
  filename: function (req, file, cb) {
    console.log("---------------------------------")
    console.log("función de multer")
    console.log({file: file})
    console.log(path.extname(file.originalname))
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`) //https://stackoverflow.com/questions/31592726/how-to-store-a-file-with-file-extension-with-multer
    // console.log(cb)
    console.log("---------------------------------")
  }
})

const upload = multer({ storage: storage })

module.exports = upload
