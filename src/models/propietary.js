const mongoose = require('mongoose')

const Experience = require('./experience')
const Province = require('./province')
const Location = require('./location')
const Stay = require('./stay')
const Schema = mongoose.Schema
// Creamos el objeto del esquema con sus correspondientes campos
const propietarySchema = new Schema({
  propietaryName: String,
  languages: [String],
  urlname: String,
  email: String,
  phoneNumber: String,
  proptietaryText: String,
  bio: String,
  images: [{}],
  webSite: String,
  social: {String},
  active: Boolean
},
  {
    timestamps: true
  })

// propietarySchema.methods.setImgUrl = function setImgURL(filename) {
//   console.log(filename)
//   const { host, port } = appConfig
//   this.imageUrl.push( `${host}:${port}/public/${filename}`)
// }
// Exportamos el modelo para usarlo en otros ficheros
console.log('entrando al mongoose de propietary')
module.exports = mongoose.model('Propietary', propietarySchema)
