// Cargamos el módulo de mongoose
const mongoose = require('mongoose')
// // Cargamos el módulo de bcrypt
// const bcrypt = require('bcrypt');
// // Definimos el factor de costo, el cual controla cuánto tiempo se necesita para calcular un solo hash de BCrypt. Cuanto mayor sea el factor de costo, más rondas de hash se realizan. Cuanto más tiempo sea necesario, más difícil será romper el hash con fuerza bruta.
// const saltRounds = 10;
// Definimos los esquemas
const Propietary = require('./propietary')
const Province = require('./province')
const Location = require('./location')
const Stay = require('./stay')
const Experience = require('./experience')
const Schema = mongoose.Schema
// Creamos el objeto del esquema con sus correspondientes campos
const commercialSchema = new Schema({
    commercialName: String,
    commercialType: String,
    commercialSubType: String,
    urlCommercialName: String,
    email: String,
    phoneNumber: String,
    address: String,
    contactWhatsApp: String,
    propietary: {type: Schema.ObjectId, ref: Propietary},
    commercialMainText: String,
    comercialSubText: String,
    hoursOpen: String,
    images: [{}],
    webSite: String,
    social: [String],
    province: {type: Schema.ObjectId, ref: Province},
    location: {type: Schema.ObjectId, ref: Location},
    stays: [ {type: Schema.ObjectId, ref: Stay} ],
    experiences: [ {type: Schema.ObjectId, ref: Experience} ],
    geoLocation: {},
    active: Boolean
},
    {
        timestamps: true
    })

// Exportamos el modelo para usarlo en otros ficheros
console.log('entrando al mongoose de commercial')
module.exports = mongoose.model('Commercial', commercialSchema)
