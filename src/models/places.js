// Cargamos el módulo de mongoose
const mongoose = require('mongoose')
const Province = require('./province')
const Location = require('./location')

const Schema = mongoose.Schema
// Creamos el objeto del esquema con sus correspondientes campos
const placeSchema = new Schema({
    placeName: String,
    placeNameUrl: String,
    images: [{}],
    mainTextplace: String,
    subTextPlace: String,
    province: {type: Schema.ObjectId, ref: Province},
    location: {type: Schema.ObjectId, ref: Location},
    active: Boolean
},
    {
        timestamps: true
    })

// Exportamos el modelo para usarlo en otros ficheros
console.log('entrando al mongoose de places')
module.exports = mongoose.model('Places', placeSchema)