const mongoose = require('mongoose')
// const { appConfig } = require('../config/config')

const Propietary = require('./propietary')
const Province = require('./province')
const Location = require('./location')


// Definimos los esquemas
const Schema = mongoose.Schema
// Creamos el objeto del esquema con sus correspondientes campos
const staySchema = new Schema({
  stayType: String,
  stayName: String,
  urlStayName: String,
  mainText: String,
  subText: String,
  ocupancy: Number,
  beds: Number,
  rooms: Number,
  kitchen: Boolean,
  microWave: Boolean,
  oven: Boolean,
  grills: Boolean,
  washingMachine: Boolean,
  wifi: Boolean,
  parking: Boolean,
  airConditioning: Boolean,
  heating: Boolean,
  smartTV: Boolean,
  fridge: Boolean,
  breakfast: Boolean,
  plate: Boolean,
  laundryService: Boolean,
  checkIn: String,
  checkOut: String,
  armchair: Boolean,
  images: [{}],
  province: {type: Schema.ObjectId, ref: Province},
  location: {type: Schema.ObjectId, ref: Location},
  active: Boolean,
},
  {
    timestamps: true
  })

// Exportamos el modelo para usarlo en otros ficheros
console.log('entrando al mongoose de stay')
module.exports = mongoose.model('Stay', staySchema)
