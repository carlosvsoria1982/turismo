// Cargamos el módulo de mongoose
const mongoose = require('mongoose')
const Province = require('./province')
const Location = require('./location')
const Places = require('./places')
const Schema = mongoose.Schema
// Creamos el objeto del esquema con sus correspondientes campos
const experienceSchema = new Schema({
    experienceName: String,
    experienceType: String,
    experienceNameUrl: String,
    images: [{}],
    experienceMainText: String,
    experienceSubText: String,
    review: String,
    ageForExperience: Number,
    experienceTime: Number,
    province: {type: Schema.ObjectId, ref: Province},
    location: {type: Schema.ObjectId, ref: Location},
    place: {type: Schema.ObjectId, ref: Places},
    active: Boolean
},
    {
        timestamps: true
    })

// Exportamos el modelo para usarlo en otros ficheros
console.log('entrando al mongoose de experience')
module.exports = mongoose.model('Experience', experienceSchema)
