const { response } = require('express')
// Cargamos el módulo de mongoose
const mongoose = require('mongoose')
const { appConfig } = require('../config/config')
const Location = require('./location')

const Schema = mongoose.Schema
// Creamos el objeto del esquema con sus correspondientes campos
const provinceSchema = new Schema({
    provinceName: String,
    urlName: String,
    images: [{}],
    mainTextProvince: String,
    subtittleProvince: String,
    stays: Number,
    experiences: Number,
    places: Number,
    geoLocation: {String},
    location: {type: Schema.ObjectId, ref: Location}},
    {
        timestamps: true
    })

provinceSchema.methods.setImgUrl = function setImgURL(filename) {
        console.log(filename)
        const { host, port } = appConfig
        this.imageUrl.push( `${host}:${port}/public/${filename}`)
      }

// Exportamos el modelo para usarlo en otros ficheros
console.log('entrando al mongoose de province')
module.exports = mongoose.model('Province', provinceSchema)
