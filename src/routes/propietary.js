// Cargamos el modulo express
const express = require('express')
const upload = require('../libs/storage')
const router = express.Router()
const { indexFunc, AddPropietary, deletePropietary, getOnePropietary, addImage, updatePropietary } = require('../controllers/propietary')

router.get('/propietary', indexFunc)
// router.get('/propietary/:id', getOnePropietary)
router.post('/propietary', AddPropietary)
router.delete('/propietary/:id', deletePropietary)
router.post('/propietary/image/:id', upload.single('image'), addImage)
router.post('/propietary/update/:id',updatePropietary)
module.exports = router
