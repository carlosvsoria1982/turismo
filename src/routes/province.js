// Cargamos el modulo express
const express = require('express')
const upload = require('../libs/storage')
const router = express.Router()
const { province, indexProvince, addProvince, deleteProvince, updateProvince, addImage, setData} = require('../controllers/province')

// const propietary = require('../models/propietary');
// // Cargamos el controlador del usuario
// const userController = require('../src/controllers/users');
// // Especificamos nuestras rutas teniendo en cuenta los metodos creados en nuestro controlador, y especificando que seran rutas que usaran el metodo POST
// router.post('/register', userController.create);
// //router.post('/authenticate', userController.authenticate);
// console.log({index: index})
// router.get('/propietary', index)
router.post('/province/get', indexProvince)  //obtener todas las provincias
router.get('/province/:id', province)   //obtener la provincia por medio del id
router.post('/province', upload.single('image'), addProvince)
router.delete('/province/:id', deleteProvince)

router.post('/province/image/:id', upload.single('image'), addImage)
router.post('/province/update/:id', upload.single('image'), updateProvince)
router.post('/province/:id', setData)


module.exports = router
