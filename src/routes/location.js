// Cargamos el modulo express
const express = require('express')
const upload = require('../libs/storage')
const router = express.Router()
const { indexLocation, addLocation, deleteLocation, addImage } = require('../controllers/location')

router.get('/location', indexLocation)
router.post('/location', upload.single('image'), addLocation)
router.delete('/location/:id', deleteLocation)
router.post('/location/image/:id', upload.single('image'), addImage)

module.exports = router
