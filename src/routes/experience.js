// Cargamos el modulo express
const express = require('express')
const upload = require('../libs/storage')
const router = express.Router()
const { indexExperience, addExperience, deleteExperience, addImage, setData } = require('../controllers/experience')

router.get('/experience', indexExperience)
router.post('/experience', upload.single('image'), addExperience)
router.delete('/experience/:id', deleteExperience)
router.post('/experience/image/:id', upload.single('image'), addImage)
router.post('/experience/:id', setData)
module.exports = router
