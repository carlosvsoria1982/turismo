// Cargamos el modulo express
const express = require('express')
const upload = require('../libs/storage')
const router = express.Router()
const { indexStay, addStay, deleteStay, addImage, updateStay } = require('../controllers/stay')

router.get('/stay', indexStay)
router.post('/stay', upload.single('image'), addStay)
router.delete('/stay/:id', deleteStay)
router.post('/stay/image/:id', upload.single('image'), addImage)
router.post('/stay/update/:id', updateStay)


module.exports = router
