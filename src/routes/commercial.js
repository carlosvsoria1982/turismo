// Cargamos el modulo express
const express = require('express')
const upload = require('../libs/storage')
const router = express.Router()
const { indexCommercial,  deleteCommercial, addCommercial, setData, addImage, updateCommercial, test } = require('../controllers/commercial')


router.post('/commercial/get', indexCommercial)
// router.post('/commercial', indexCommercial)

router.post('/commercial/add', addCommercial)
router.post('/commercial/:id', setData)
router.post('/commercial/update/:id', updateCommercial)

router.post('/commercial/image/:id', upload.single('image'), addImage)
router.delete('/commercial/:id', deleteCommercial)
router.get('/commercial/test', test)


module.exports = router
