// Cargamos el modulo express
const express = require('express')
const upload = require('../libs/storage')
const router = express.Router()
const { indexPlaces, addPlaces, deletePlaces } = require('../controllers/places')

router.get('/location', indexPlaces)
router.post('/location', upload.single('image'), addPlaces)
router.delete('/location/:id', deletePlaces)

module.exports = router