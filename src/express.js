const express = require('express')
const propietaryRoute = require('./routes/propietary')
const provinceRoute =require('./routes/province')
const commercialRoute =require('./routes/commercial')
const locationRoute =require('./routes/location')
const experienceRoute =require('./routes/experience')
const stayRoute =require('./routes/stay')
const bodyParser = require('body-parser')   // se utiliza para para parsear los objetos json
const cors = require('cors')
const app = express()
app.use(cors())

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json()) // for parsing application/json


app.use('/v1',propietaryRoute)
app.use('/v1',provinceRoute)
app.use('/v1',commercialRoute)
app.use('/v1',locationRoute)
app.use('/v1',experienceRoute)
app.use('/v1',stayRoute)

app.use('/public', express.static(`${__dirname}/storage/imgs`))

// app.use('/v1',propietaryRoute)

module.exports = app